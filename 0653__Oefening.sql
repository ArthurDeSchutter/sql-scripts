USE aptunes;
DROP PROCEDURE IF EXISTS GetAlbumDuration;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=student@localhost  PROCEDURE `GetAlbumDuration2`(IN albumId INT, OUT totalLength SMALLINT)
BEGIN

    DECLARE ok INTEGER DEFAULT 0;
    DECLARE songLength TINYINT UNSIGNED;

    DECLARE currentSong
        CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = albumId;


    DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET ok = 1;

    OPEN currentSong;
    SET totalLength = 0;
    getLength:
    LOOP
        FETCH currentSong INTO songLength;
        IF ok = 1
        THEN
            LEAVE getLength;
        END IF;
        SET totalLength = totalLength + songLength;
    END LOOP;

    CLOSE currentSong;
END$$

DELIMITER ;

CALL GetAlbumDuration(12, @totalLength);
SELECT @totalLength;