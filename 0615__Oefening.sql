ALTER VIEW auteursboeken
AS
SELECT CONCAT(personen.Voornaam,' ',personen.Familienaam) AS Auteur, boeken.Titel,boeken.id as boeken_id
FROM
	boeken
INNER JOIN
	publicaties 
ON publicaties.Boeken_Id = boeken.id
INNER JOIN 
	personen
ON
	publicaties.Personen_Id = personen.Id;