ALTER TABLE baasjes
ADD COLUMN huisdieren_Id INT,
ADD CONSTRAINT fk_baasjes_huisdieren
  FOREIGN KEY (huisdieren_Id)
  REFERENCES huisdieren(Id);