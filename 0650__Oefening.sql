CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
	DECLARE counter INT DEFAULT 0;
	DECLARE	 Rand INT DEFAULT FLOOR((RAND() * (3-1))+1); -- danku stackoverflow ->FLOOR( RAND() * (max-min) + min )
	DECLARE CONTINUE HANDLER FOR SQLSTATE '45002' RESIGNAL SET MESSAGE_TEXT =  "State 45002 opgevangen. Geen probleem.";    
	DECLARE EXIT HANDLER FOR 1062 RESIGNAL SET MESSAGE_TEXT =  "waarde zit al in de tabel";   



	IF  counter < 3 THEN
		CALL MockAlbumReleases(1);
	ELSEIF counter = 3 THEN
		IF Rand = 3 THEN -- give error
			signal sqlstate '45002';
		ELSE
			CALL MockAlbumReleases(1);
		END IF;
        
	END IF;

END
