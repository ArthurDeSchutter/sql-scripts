USE ModernWays;
ALTER TABLE huisdieren ADD COLUMN Geluid varchar(20) char set utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Geluid = 'woef' WHERE soort = 'hond';
SET SQL_SAFE_UPDATES = 1;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Geluid = 'miauw' WHERE soort = 'kat';
SET SQL_SAFE_UPDATES = 1;