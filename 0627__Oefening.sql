USE modernways;
SELECT
	Voornaam
FROM
	studenten
WHERE
	LENGTH(voornaam) < (SELECT AVG(LENGTH(Voornaam)) FROM studenten);