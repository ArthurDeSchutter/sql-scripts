CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(IN endDate DATE,OUT numberCleaned INT)
BEGIN
	SELECT COUNT(Einddatum)
    INTO numberCleaned
    FROM lidmaatschappen
    WHERE Einddatum < endDate;
    
    SET SQL_SAFE_UPDATES = 0;
	DELETE
    FROM lidmaatschappen
    WHERE Einddatum < endDate;
        SET SQL_SAFE_UPDATES = 1;

END