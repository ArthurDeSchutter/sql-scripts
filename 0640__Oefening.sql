CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(IN findTitel varchar(100))
BEGIN
	SELECT *
    FROM liedjes
    where Titel = findTitel;
END