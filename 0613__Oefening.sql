CREATE VIEW AuteursBoekenauteursboeken
AS
SELECT CONCAT(personen.Voornaam,' ',personen.Familienaam) AS Auteur, boeken.Titel
FROM
	boeken
INNER JOIN
	publicaties 
ON publicaties.Boeken_Id = boeken.id
INNER JOIN 
	personen
ON
	publicaties.Personen_Id = personen.Id;