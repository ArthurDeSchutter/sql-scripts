USE ModernWays;

CREATE TABLE `Uitleningen` (
  leden_Id int(11) NOT NULL,
  Boeken_Id int(11) NOT NULL,
  Startdatum DATE,
  Einddatum DATE,
  CONSTRAINT fk_uitleningen_leden FOREIGN KEY (leden_Id)
  REFERENCES leden(Id),
  CONSTRAINT fk_uitleningen_boeken FOREIGN KEY (boeken_Id)
  REFERENCES boeken(Id)
)
	