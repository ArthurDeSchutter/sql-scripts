CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumRelease`()
BEGIN
DECLARE 
	numberOfAlbums,
	numberOfBands,
	randomAlbumId,
	randomBandId 
    INT DEFAULT 0;

	SET numberOfAlbums = (SELECT MAX(Id) FROM albums);
	SET numberOfBands = (SELECT MAX(Id) FROM bands);
	SET randomAlbumId = (FLOOR(RAND() * numberOfAlbums)+1);
	SET randomBandId = (FLOOR(RAND() * numberOfBands)+1);
    		INSERT INTO 
			albumreleases(Albums_id,Bands_id)
        VALUES
			(randomAlbumId,randomBandId);

    
END