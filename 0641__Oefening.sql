CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(OUT  Aantal TINYINT)
BEGIN
	SELECT count(Naam)
    INTO Aantal
    FROM genres;
END