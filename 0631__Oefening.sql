USE modernways;
SELECT
	voornaam
FROM
	studenten
WHERE
	studenten.voornaam in (SELECT Voornaam FROM personeelsleden)
    AND
	studenten.voornaam in (SELECT Voornaam FROM directieleden)
