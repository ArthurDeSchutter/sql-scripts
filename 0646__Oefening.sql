CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleases`(IN extraReleases INT)
BEGIN
	DECLARE
		counter
		INT DEFAULT 0;
        
	REPEAT
		CALL MockAlbumReleaseWithSuccess(@succes);
        set counter = counter +1;
	UNTIL  counter = extraReleases
END REPEAT;
END