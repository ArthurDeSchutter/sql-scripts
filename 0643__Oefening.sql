CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(IN insertTitel varchar(100),IN insertBands_id INT)
BEGIN
-- -----------------
INSERT INTO albums (
	Titel
)
VALUES (
	insertTitel
);
-- ----------------------
INSERT INTO albumreleases (
	bands_id,
    albums_id
)
VALUES (
	insertBands_id,
	LAST_INSERT_ID()
);

END