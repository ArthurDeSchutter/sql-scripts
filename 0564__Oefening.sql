USE ModernWays;
CREATE TABLE Student(
Voornaam VARCHAR(100) CHAR SET utf8mb4,
Familienaam VARCHAR(100) CHAR SET utf8mb4,
Studentennummer INT PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE Opleidingen(
Naam VARCHAR(100) CHAR SET utf8mb4 PRIMARY KEY
);
ALTER TABLE Student
ADD COLUMN Opleidingen_Naam VARCHAR(100) CHAR SET utf8mb4 not null,
ADD COLUMN Semester TINYINT UNSIGNED,
ADD CONSTRAINT fk_Student FOREIGN KEY (Opleidingen_Naam)
REFERENCES Opleidingen(Naam);



CREATE TABLE Vak(
Naam VARCHAR(100) CHAR SET utf8mb4 PRIMARY KEY
);

CREATE TABLE Lector(
Voornaam VARCHAR(100) CHAR SET utf8mb4,
Familienaam VARCHAR(100) CHAR SET utf8mb4,
Personeelnummer INT PRIMARY KEY AUTO_INCREMENT
);
CREATE TABLE Opleidingsonderdeel(
Semester TINYINT UNSIGNED,
Opleidingen_naam VARCHAR(100) not null,
CONSTRAINT fk_Opleidingsonderdeel_Opleidingen FOREIGN KEY (Opleidingen_naam)
REFERENCES Opleidingen(Naam),
Vak_naam VARCHAR(100) not null,
CONSTRAINT fk_Vak_Naam FOREIGN KEY (Vak_Naam)
REFERENCES Vak(Naam)
);
CREATE TABLE Geeft_Vak(
lector_Personeelnummer int not null,
CONSTRAINT fk_lector_Personeelnummer FOREIGN KEY (lector_personeelnummer)
REFERENCES lector(Personeelnummer),
Vak_Naam VARCHAR(100) not null,
CONSTRAINT fk_Geeft_Vak_Naam FOREIGN KEY (Vak_Naam)
REFERENCES Vak(Naam)
);