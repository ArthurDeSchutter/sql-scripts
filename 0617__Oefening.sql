
CREATE VIEW AuteursBoekenRatings
AS
SELECT Auteur,Titel,rating
from gemiddelderatings
inner join
	auteursboeken
ON 
	auteursboeken.boeken_id = gemiddelderatings.boeken_id