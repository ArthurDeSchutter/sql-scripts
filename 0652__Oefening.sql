USE `aptunes`;
CREATE USER 'student'
IDENTIFIED BY 'ikbeneenstudent';

GRANT EXECUTE ON aptunes.GetAlbumDuration
TO student;