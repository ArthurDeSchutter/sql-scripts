CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
	DECLARE	 Rand INT DEFAULT FLOOR((RAND() * (3-1))+1); -- danku stackoverflow ->FLOOR( RAND() * (max-min) + min )
			DECLARE EXIT HANDLER FOR SQLSTATE '45001' SELECT "State 45002 opgevangen. Geen probleem.";
			DECLARE EXIT HANDLER FOR SQLSTATE '45002' SELECT "Een algemene fout opgevangen.";

		IF  Rand = 1 THEN

			signal sqlstate '45001';
		ELSEIF Rand = 2 THEN
			signal sqlstate '45002';
		END IF;
END