CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleaseWithSuccess`(OUT success BOOL)
BEGIN
DECLARE 
	numberOfAlbums,
	numberOfBands,
	randomAlbumId,
	randomBandId
    INT DEFAULT 0;

	SET numberOfAlbums = (SELECT MAX(Id) FROM albums);
	SET numberOfBands = (SELECT MAX(Id) FROM bands);
	SET randomAlbumId = (FLOOR(RAND() * numberOfAlbums)+1);
	SET randomBandId = (FLOOR(RAND() * numberOfBands)+1);
		
    IF (SELECT Bands_id FROM aptunes.albumreleases WHERE Bands_id = randomBandId) AND (SELECT Albums_id FROM aptunes.albumreleases WHERE Albums_id = randomAlbumId) IS  NULL
        THEN
        INSERT INTO 
			albumreleases(Albums_id,Bands_id)
        VALUES
			(randomAlbumId,randomBandId);
		SET success = 1;

    
	ELSE		
		SET success = 1;
	END IF;
 END