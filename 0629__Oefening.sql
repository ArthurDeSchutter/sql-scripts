USE modernways;

SELECT
	studenten_id
FROM
	evaluaties
    
inner join studenten
on  evaluaties.studenten_id = studenten.id
group by evaluaties.studenten_Id
HAVING AVG(Cijfer) > (SELECT AVG(Cijfer) FROM Evaluaties);