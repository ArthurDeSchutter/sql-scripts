CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleases`(IN extraReleases INT)
BEGIN
	DECLARE
		counter
		INT DEFAULT 0;
        
	loop_demo:  LOOP
		IF  counter < extraReleases THEN
			CALL MockAlbumReleaseWithSuccess(@succes);
			set counter = counter +1;
        END IF;
	END LOOP;
END