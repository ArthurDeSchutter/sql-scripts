SELECT `leden`.`Voornaam`, `boeken`.`Titel`
FROM uitleningen
     INNER JOIN leden ON `leden`.`Id` = `uitleningen`.`leden_Id`
     INNER JOIN boeken ON `boeken`.`Id` = `uitleningen`.`Boeken_Id`